package dwarffortress.tfc;

import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Random;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.registry.GameRegistry;

import com.bioxx.tfc.api.Enums.EnumFoodGroup;
import com.bioxx.tfc.api.Enums.RuleEnum;
import com.bioxx.tfc.api.TFCBlocks;
import com.bioxx.tfc.api.TFCItems;
import com.bioxx.tfc.api.Constant.Global;
import com.bioxx.tfc.api.Crafting.*;
import com.bioxx.tfc.api.HeatRegistry;
import com.bioxx.tfc.api.HeatIndex;
import com.bioxx.tfc.api.TFCFluids;
import com.bioxx.tfc.Food.*;
import com.bioxx.tfc.Core.TFCTabs;
import com.bioxx.tfc.Core.FluidBaseTFC;
import com.bioxx.tfc.Core.Recipes;
import com.bioxx.tfc.Items.ItemTerra;
import com.bioxx.tfc.Items.ItemAlcohol;
import com.peffern.crops.BaseCrop;
import com.peffern.crops.CropsRegistry;
import com.peffern.crops.RenderCustomCrop;
import com.peffern.crops.ICrop;

import terramisc.core.TFCMItems;

import com.fluffy2.amnesiadecorations.AmnesiaDecorations;

import dwarffortress.tfc.DwarfFortress;
import dwarffortress.tfc.Items.*;

public class DFItems
{

	public static Item plumphelmetSeeds;
	public static Item plumphelmet;
	public static Item boiledmushrooms;
	public static Item sweetpodSeeds;
	public static Item sweetpod;

	public static Item cavewheatSeeds;
	public static Item cavewheatWhole;
	public static Item cavewheatGrain;
	public static Item cavewheatGround;
	public static Item cavewheatDough;
	public static Item cavewheatBread;

	public static Item pigtailSeeds;
	public static Item dimplecupSeeds;

	public static Item pigtail;
	public static Item dimplecup;
	public static Item pigtailCloth;
	public static Item soap;

	public static Item dwarvenwine;
	public static Item dwarvenrum;
	public static Item dwarvenbeer;
	public static Item dwarvenale;

	public static Item spiderRaw;
	public static Item boiledSpider;
	public static Item cookie;

	public static Fluid DWARVENWINE;
	public static Fluid DWARVENRUM;
	public static Fluid DWARVENBEER;
	public static Fluid DWARVENALE;
	public static Fluid LYEOIL;
	public static Fluid SOAP;

	public static Item medicineBag;

	public static void setup()
	{
		plumphelmet = new ItemFoodTFC(EnumFoodGroup.Vegetable, 0, 0, 10, 15, 20, true).setUnlocalizedName("Plumphelmet");
		boiledmushrooms = new ItemFoodTFC(EnumFoodGroup.Vegetable, 0, 0, 10, 15, 20, true).setUnlocalizedName("Boiled Mushrooms");
		sweetpod = new ItemFoodTFC(EnumFoodGroup.None, 20, 0, 0, 0, 0, false).setUnlocalizedName("Sweetpod");

		cavewheatWhole = new ItemFoodTFC(EnumFoodGroup.Grain, 10, 0, 0, 5, 20, false, false).setFolder("food/").setUnlocalizedName("Cavewheat Whole");
		cavewheatGrain = new ItemFoodTFC(EnumFoodGroup.Grain, 10, 0, 0, 5, 20).setDecayRate(0.5f).setUnlocalizedName("Cavewheat Grain");
		cavewheatGround = new ItemFoodTFC(EnumFoodGroup.Grain, 10, 0, 0, 0, 20, false, false).setFolder("food/").setUnlocalizedName("Cavewheat Ground");
		cavewheatDough = new ItemFoodTFC(EnumFoodGroup.Grain, 10, 0, 0, 0, 20, false, false).setUnlocalizedName("Cavewheat Dough");
		cavewheatBread = new ItemFoodTFC(EnumFoodGroup.Grain, 10, 0, 0, 0, 20).setUnlocalizedName("Cavewheat Bread");

		cookie = new ItemFoodTFC(EnumFoodGroup.Grain, 100, 0, 0, 0, 20).setUnlocalizedName("Dwarven Cookie");

		pigtail = new ItemTerra().setFolder("plants/").setUnlocalizedName("Pigtail").setCreativeTab(TFCTabs.TFC_MATERIALS);
		dimplecup = new ItemTerra().setFolder("plants/").setUnlocalizedName("Dimplecup").setCreativeTab(TFCTabs.TFC_MATERIALS);
		pigtailCloth = new ItemTerra().setUnlocalizedName("PigtailCloth").setCreativeTab(TFCTabs.TFC_MATERIALS);
		soap = new ItemSoap().setUnlocalizedName("Soap").setCreativeTab(TFCTabs.TFC_MATERIALS);

		dwarvenwine = new ItemAlcohol().setUnlocalizedName("Dwarven Wine").setCreativeTab(TFCTabs.TFC_FOODS);
		dwarvenrum = new ItemAlcohol().setUnlocalizedName("Dwarven Rum").setCreativeTab(TFCTabs.TFC_FOODS);
		dwarvenbeer = new ItemAlcohol().setUnlocalizedName("Dwarven Beer").setCreativeTab(TFCTabs.TFC_FOODS);
		dwarvenale = new ItemAlcohol().setUnlocalizedName("Dwarven Ale").setCreativeTab(TFCTabs.TFC_FOODS);

		spiderRaw = new ItemFoodMeat(EnumFoodGroup.Protein, 0, 0, 0, 0, 30, false, false).setDecayRate(2.5f).setCanSmoke().setHasCookedIcon().setSmokeAbsorbMultiplier(1F).setUnlocalizedName("Spider");
		boiledSpider = new ItemFoodMeat(EnumFoodGroup.Protein, 0, 0, 0, 0, 30, false, false).setDecayRate(3.0f).setUnlocalizedName("Boiled Spider");

		medicineBag = new ItemMedicineBag().setUnlocalizedName("Medicine Bag").setCreativeTab(TFCTabs.TFC_TOOLS);

		GameRegistry.registerItem(plumphelmet, plumphelmet.getUnlocalizedName());
		GameRegistry.registerItem(boiledmushrooms, boiledmushrooms.getUnlocalizedName());
		GameRegistry.registerItem(sweetpod, sweetpod.getUnlocalizedName());

		GameRegistry.registerItem(cavewheatWhole, cavewheatWhole.getUnlocalizedName());
		GameRegistry.registerItem(cavewheatGrain, cavewheatGrain.getUnlocalizedName());
		GameRegistry.registerItem(cavewheatGround, cavewheatGround.getUnlocalizedName());
		GameRegistry.registerItem(cavewheatDough, cavewheatDough.getUnlocalizedName());
		GameRegistry.registerItem(cavewheatBread, cavewheatBread.getUnlocalizedName());

		GameRegistry.registerItem(pigtail, pigtail.getUnlocalizedName());
		GameRegistry.registerItem(dimplecup, dimplecup.getUnlocalizedName());
		GameRegistry.registerItem(pigtailCloth, pigtailCloth.getUnlocalizedName());
		GameRegistry.registerItem(soap, soap.getUnlocalizedName());

		GameRegistry.registerItem(dwarvenwine, dwarvenwine.getUnlocalizedName());
		GameRegistry.registerItem(dwarvenrum, dwarvenrum.getUnlocalizedName());
		GameRegistry.registerItem(dwarvenbeer, dwarvenbeer.getUnlocalizedName());
		GameRegistry.registerItem(dwarvenale, dwarvenale.getUnlocalizedName());

		GameRegistry.registerItem(spiderRaw, spiderRaw.getUnlocalizedName());
		GameRegistry.registerItem(boiledSpider, boiledSpider.getUnlocalizedName());
		GameRegistry.registerItem(cookie, cookie.getUnlocalizedName());

		GameRegistry.registerItem(medicineBag, medicineBag.getUnlocalizedName());
	}

	public static void setupRecipes()
	{
		Recipes.addFoodRefineRecipe(cavewheatWhole, cavewheatGrain);

		Recipes.addFoodDoughRecipe(cavewheatGround, cavewheatDough, TFCItems.woodenBucketWater);
		Recipes.addFoodDoughRecipe(cavewheatGround, cavewheatDough, TFCItems.redSteelBucketWater);
		Recipes.addFoodSaltRecipe(spiderRaw);

		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(medicineBag, 1), "chestWood", "flower1", "strongAlcohol", "soap", "materialCloth"));

		GameRegistry.addRecipe(new ItemStack(AmnesiaDecorations.ItemAmnesiaDoor, 1, 1), "WW", "WP", "WW", 'W', new ItemStack(TFCItems.singlePlank, 1), 'P', new ItemStack(TFCItems.stick, 1));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(AmnesiaDecorations.ItemAmnesiaDoor, 3, 0), "WW", "WP", "WW", 'W', "plankWood", 'P', "ingotAnyBronze"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(AmnesiaDecorations.ItemAmnesiaDoor, 3, 2), "WW", "WP", "WW", 'W', "stone", 'P', "ingotAnyBronze"));
	}

	public static void setupAnvilRecipes(Random r, World world)
	{
		AnvilManager manager = AnvilManager.getInstance();
		AnvilManager.world = world;

		manager.addPlan("keys", new PlanRecipe(new RuleEnum[]{RuleEnum.BENDLAST, RuleEnum.BENDSECONDFROMLAST, RuleEnum.DRAWTHIRDFROMLAST}));

		manager.addRecipe(new AnvilRecipe(new ItemStack(TFCItems.bronzeIngot), null,"keys", AnvilReq.COPPER, new ItemStack(AmnesiaDecorations.Key, 3)));
		manager.addRecipe(new AnvilRecipe(new ItemStack(TFCItems.bismuthBronzeIngot), null,"keys", AnvilReq.COPPER, new ItemStack(AmnesiaDecorations.Key, 3)));
		manager.addRecipe(new AnvilRecipe(new ItemStack(TFCItems.blackBronzeIngot), null,"keys", AnvilReq.COPPER, new ItemStack(AmnesiaDecorations.Key, 3)));
	}

	public static void setupQuernRecipes()
	{
		QuernManager manager = QuernManager.getInstance();

		manager.addRecipe(new QuernRecipe(new ItemStack(cavewheatGrain, 1), new ItemStack(cavewheatGround, 1)));
		manager.addRecipe(new QuernRecipe(new ItemStack(dimplecup, 1), new ItemStack(TFCItems.dye, 2, 4)));
	}

	public static void setupHeatRecipes()
	{
		HeatRegistry manager = HeatRegistry.getInstance();

		manager.addIndex(new HeatIndex(new ItemStack(spiderRaw, 1), 1, 1200, null));
		manager.addIndex(new HeatIndex(new ItemStack(cavewheatDough, 1), 1, 600, new ItemStack(cavewheatBread, 1)).setKeepNBT(true));
		manager.addIndex(new HeatIndex(new ItemStack(plumphelmet, 1), 1, 1200, null));
	}

	public static void setupFluids()
	{
		DWARVENWINE = new FluidBaseTFC("dwarvenwine").setBaseColor(0x9619DD);
		FluidRegistry.registerFluid(DWARVENWINE);
		FluidContainerRegistry.registerFluidContainer(new FluidStack(DWARVENWINE, 250), new ItemStack(dwarvenwine), new ItemStack(TFCItems.glassBottle));

		DWARVENRUM = new FluidBaseTFC("dwarvenrum").setBaseColor(0xE6EC91);
		FluidRegistry.registerFluid(DWARVENRUM);
		FluidContainerRegistry.registerFluidContainer(new FluidStack(DWARVENRUM, 250), new ItemStack(dwarvenrum), new ItemStack(TFCItems.glassBottle));

		DWARVENBEER = new FluidBaseTFC("dwarvenbeer").setBaseColor(0xE5DA00);
		FluidRegistry.registerFluid(DWARVENBEER);
		FluidContainerRegistry.registerFluidContainer(new FluidStack(DWARVENBEER, 250), new ItemStack(dwarvenbeer), new ItemStack(TFCItems.glassBottle));

		DWARVENALE = new FluidBaseTFC("dwarvenale").setBaseColor(0xF0EDB4);
		FluidRegistry.registerFluid(DWARVENALE);
		FluidContainerRegistry.registerFluidContainer(new FluidStack(DWARVENALE, 250), new ItemStack(dwarvenale), new ItemStack(TFCItems.glassBottle));

		LYEOIL = new FluidBaseTFC("lyeoil").setBaseColor(0xF2E7AB);
		FluidRegistry.registerFluid(LYEOIL);

		SOAP = new FluidBaseTFC("soap").setBaseColor(0xFDF3B5);
		FluidRegistry.registerFluid(SOAP);
	}

	public static void setupBarrelRecipes()
	{
		BarrelManager.getInstance().addRecipe(new BarrelAlcoholRecipe(ItemFoodTFC.createTag(new ItemStack(plumphelmet), 320), new FluidStack(TFCFluids.FRESHWATER, 10000), null, new FluidStack(DWARVENWINE, 10000), 100));
		BarrelManager.getInstance().addRecipe(new BarrelAlcoholRecipe(ItemFoodTFC.createTag(new ItemStack(TFCItems.sugar), 320), new FluidStack(TFCFluids.FRESHWATER, 10000), null, new FluidStack(DWARVENRUM, 10000), 160));
		BarrelManager.getInstance().addRecipe(new BarrelAlcoholRecipe(ItemFoodTFC.createTag(new ItemStack(cavewheatGround), 320), new FluidStack(TFCFluids.FRESHWATER, 10000), null, new FluidStack(DWARVENBEER, 10000), 144));

		BarrelManager.getInstance().addRecipe(new BarrelRecipe(new ItemStack(pigtail, 1), new FluidStack(TFCFluids.TANNIN, 1000), null, new FluidStack(DWARVENALE, 1000), 43).setAllowAnyStack(false));
		BarrelManager.getInstance().addRecipe(new BarrelMultiItemRecipe(ItemFoodTFC.createTag(new ItemStack(sweetpod), 1), new FluidStack(TFCFluids.FRESHWATER, 60), ItemFoodTFC.createTag(new ItemStack(TFCItems.sugar), 1), new FluidStack(TFCFluids.FRESHWATER, 60)).setMinTechLevel(0));
		BarrelManager.getInstance().addRecipe(new BarrelMultiItemRecipe(new ItemStack(TFCMItems.itemBowlTallow, 1), new FluidStack(TFCFluids.LYE, 750), new ItemStack(soap,  1), new FluidStack(TFCFluids.LYE, 750)).setKeepStackSize(false));
		BarrelManager.getInstance().addRecipe(new BarrelLiquidToLiquidRecipe(new FluidStack(TFCFluids.LYE, 9000), new FluidStack(TFCFluids.OLIVEOIL, 1000), new FluidStack(LYEOIL, 10000)).setSealedRecipe(false).setMinTechLevel(0).setRemovesLiquid(false));
		BarrelManager.getInstance().addRecipe(new BarrelLiquidToLiquidRecipe(new FluidStack(LYEOIL, 9000), new FluidStack(TFCFluids.OLIVEOIL, 1000), new FluidStack(LYEOIL, 10000)).setSealedRecipe(false).setMinTechLevel(0).setRemovesLiquid(false));
		BarrelManager.getInstance().addRecipe(new BarrelRecipe(null, new FluidStack(LYEOIL, 10000), null, new FluidStack(SOAP, 10000), 43).setRemovesLiquid(false));
		BarrelManager.getInstance().addRecipe(new BarrelRecipe(new ItemStack(TFCItems.ceramicMold, 1, 1), new FluidStack(SOAP, 1000), new ItemStack(soap,  1), new FluidStack(SOAP, 1000), 0).setMinTechLevel(0).setSealedRecipe(false));

		BarrelManager.getInstance().addRecipe(new BarrelVinegarRecipe(new FluidStack(DWARVENWINE, 100), new FluidStack(TFCFluids.VINEGAR, 100)));
		BarrelManager.getInstance().addRecipe(new BarrelVinegarRecipe(new FluidStack(DWARVENRUM, 100), new FluidStack(TFCFluids.VINEGAR, 100)));
		BarrelManager.getInstance().addRecipe(new BarrelVinegarRecipe(new FluidStack(DWARVENBEER, 100), new FluidStack(TFCFluids.VINEGAR, 100)));
		BarrelManager.getInstance().addRecipe(new BarrelVinegarRecipe(new FluidStack(DWARVENALE, 100), new FluidStack(TFCFluids.VINEGAR, 100)));
	}

	public static void setupCrops()
	{
		plumphelmetSeeds = CropsRegistry.addCrop(Crop("Plumphelmet", 0, 32, 7, -30, -30, 0.5f, plumphelmet, 28, "seedsPlumphelmet", "Seeds Plumphelmet", false));
		sweetpodSeeds = CropsRegistry.addCrop(Crop("Sweetpod", 2, 36, 8, -30, -30, 0.5f, sweetpod, 16, "seedsSweetpod", "Seeds Sweetpod", false));
		cavewheatSeeds = CropsRegistry.addCrop(Crop("Cavewheat", 2, 40, 8, -30, -30, 0.5f, cavewheatWhole, 13, "seedsCavewheat", "Seeds Cavewheat", false));

		pigtailSeeds = CropsRegistry.addCrop(Crop("Pigtail", 2, 36, 6, -30, -30, 0.5f, pigtail, 2, "seedsPigtail", "Seeds Pigtail", false));
		dimplecupSeeds = CropsRegistry.addCrop(Crop("Dimplecup", 0, 32, 4, -30, -30, 0.5f, dimplecup, 1, "seedsDimplecup", "Seeds Dimplecup", false));
	}

	public static void setupLoomRecipes()
	{
		LoomManager.getInstance().addRecipe(new LoomRecipe(new ItemStack(pigtail,12), new ItemStack(pigtailCloth,1)),new ResourceLocation(DwarfFortress.MODID, "textures/blocks/Pigtail.png"));
	}

	public static ICrop Crop(String name, int type, int time, int stages, int gTemp, int aTemp, float nutrient, Item crop, int amount, String seedOreName, String seedName, boolean needsSunlight)
	{
		String[] iconNames = new String[stages];
		for(int i = 1; i < stages + 1; i++)
		{
			iconNames[i - 1] = DwarfFortress.MODID + ":" + "plants/crops/" + name + " (" + i + ")";

		}

		ICrop plant = new BaseCrop(name, type, time, iconNames, gTemp, aTemp, nutrient, crop, amount, seedOreName, DwarfFortress.MODID + ":" + "food/" + seedName, seedName, needsSunlight);

		return plant;
	}

	public static void registerOreDictionary()
	{
		final int WILD = OreDictionary.WILDCARD_VALUE;

		OreDictionary.registerOre("materialCloth", new ItemStack(pigtailCloth));

		OreDictionary.registerOre("strongAlcohol", new ItemStack(dwarvenrum));
		OreDictionary.registerOre("soap", soap);

		OreDictionary.registerOre("itemMushroom", DFItems.plumphelmet);
	}
}