package dwarffortress.tfc;

import java.util.Random;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.Potion;
import net.minecraft.block.material.Material;
import net.minecraft.block.Block;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

import com.bioxx.tfc.Core.TFC_Core;
import com.bioxx.tfc.Items.Tools.ItemCustomPickaxe;
import com.bioxx.tfc.Items.Tools.ItemCustomAxe;

import cpw.mods.fml.common.eventhandler.SubscribeEvent; 
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed; 
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import udary.tfcudarymod.items.fluids.ItemGlassBottleWater;
import dwarffortress.tfc.Items.ItemMedicineBag;

public class DFEventHandler { 

	@SubscribeEvent 
    public void onBreakSpeed(BreakSpeed event)
	{ 
        if (event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() instanceof ItemCustomPickaxe)
		{ 
			if (event.block.getMaterial() == Material.rock)
			{
				event.newSpeed += event.newSpeed * TFC_Core.getSkillStats(event.entityPlayer).getSkillMultiplier("skill.mining") * 5;
			}
        }
		else if (event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() instanceof ItemCustomAxe)
		{
			if (event.block.getMaterial() == Material.wood)
			{
				event.newSpeed += event.newSpeed * TFC_Core.getSkillStats(event.entityPlayer).getSkillMultiplier("skill.woodcutting") * 5;
			}
		}
    } 

	@SubscribeEvent 
    public void BreakEvent(BreakEvent event)
	{ 
		if (event.getPlayer().getCurrentEquippedItem() != null && event.getPlayer().getCurrentEquippedItem().getItem() instanceof ItemCustomPickaxe)
		{ 
			if (event.block.getMaterial() == Material.rock)
			{
				TFC_Core.getSkillStats(event.getPlayer()).increaseSkill("skill.mining", 1);
			}
		} 
    } 

	/*
	@SubscribeEvent 
	public void onPlayerUse(PlayerInteractEvent event)
	{
	    if(event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() instanceof ItemGlassBottleWater)
	    {
	    	event.entityPlayer.addPotionEffect(new PotionEffect(2, 6000, 1));
	    }
	}
	*/

	@SubscribeEvent
	public void entityInteractEvent(EntityInteractEvent event)
	{
		if (event.target instanceof EntityPlayer && !event.entity.worldObj.isRemote && event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() instanceof ItemMedicineBag)
		{
			EntityPlayer player = (EntityPlayer)event.entityPlayer;
			EntityPlayer target = (EntityPlayer)event.target;
			Random rand = new Random();
			int skill = (int)TFC_Core.getSkillStats(player).getSkillMultiplier("skill.medicine");
			int healAmount = rand.nextInt(100 + skill) + 50 + skill;
			int debuffTime = rand.nextInt(300) - skill;
			int healTime = rand.nextInt(300) + 50 + skill;
			int healPower = (int)skill / 100;

			if (target.getHealth() < target.getMaxHealth() && !target.isPotionActive(Potion.regeneration))
			{
				target.heal(healAmount);
				target.addPotionEffect(new PotionEffect(10, healTime * 20, healPower));

				if (healPower <= 4)
					target.removePotionEffect(2);
					target.addPotionEffect(new PotionEffect(2, debuffTime * 20, 5 - healPower));
				if (healPower <= 3)
					target.removePotionEffect(4);
					target.addPotionEffect(new PotionEffect(4, debuffTime * 20, 5 - healPower));
				if (healPower <= 2)
					target.removePotionEffect(18);
					target.addPotionEffect(new PotionEffect(18, debuffTime * 20, 5 - healPower));
				if (healPower == 1)
					target.removePotionEffect(9);
					target.addPotionEffect(new PotionEffect(9, debuffTime * 20, 5 - healPower));

				TFC_Core.getSkillStats(player).increaseSkill("skill.medicine", 1);

				event.entityPlayer.getCurrentEquippedItem().damageItem(1, player);
			}
		}
	}

	@SubscribeEvent
	public void onLoadWorld(WorldEvent.Load event)
	{
		Random r = new Random();

		DFItems.setupAnvilRecipes(r, event.world);
	}
}