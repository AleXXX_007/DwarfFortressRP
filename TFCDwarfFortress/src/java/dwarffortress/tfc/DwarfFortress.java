package dwarffortress.tfc;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

import com.bioxx.tfc.api.SkillsManager;

import net.minecraftforge.common.MinecraftForge;

@Mod(name = DwarfFortress.MODNAME, modid = DwarfFortress.MODID, version = DwarfFortress.VERSION, dependencies = "after:terrafirmacraft")
public class DwarfFortress {
	public static final String MODID = "tfcdwarffortress";
	public static final String MODNAME = "DwarfFortress";
	public static final String VERSION = "1.3";

	@SidedProxy(clientSide = "dwarffortress.tfc.ClientProxy", serverSide = "dwarffortress.tfc.CommonProxy")
	public static CommonProxy proxy;

	public static DFEventHandler events; 

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		SkillsManager.instance.registerSkill("skill.mining", 10000);
		SkillsManager.instance.registerSkill("skill.medicine", 400);
		DFItems.setup();
		DFItems.setupCrops();
		DFItems.setupRecipes();
		DFItems.setupQuernRecipes();
		DFItems.setupHeatRecipes();
		DFItems.setupFluids();
		DFItems.setupBarrelRecipes();
		DFItems.setupLoomRecipes();
		DFItems.registerOreDictionary();
	}

	@EventHandler 
    public void crescendo(FMLInitializationEvent event)
	{ 
        events = new DFEventHandler(); 
        MinecraftForge.EVENT_BUS.register(events); 
    } 
}