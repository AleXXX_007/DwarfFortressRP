package com.bioxx.tfc.Core.Player;

import java.util.HashMap;
import java.util.Map;

import io.netty.buffer.ByteBuf;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

import net.minecraftforge.common.MinecraftForge;

import cpw.mods.fml.common.network.ByteBufUtils;

import com.bioxx.tfc.TerraFirmaCraft;
import com.bioxx.tfc.Core.TFC_Core;
import com.bioxx.tfc.Handlers.Network.AbstractPacket;
import com.bioxx.tfc.Handlers.Network.PlayerUpdatePacket;
import com.bioxx.tfc.api.SkillsManager;
import com.bioxx.tfc.api.SkillsManager.Skill;
import com.bioxx.tfc.api.Events.GetSkillMultiplierEvent;
import com.bioxx.tfc.api.Events.PlayerSkillEvent;

public class SkillStats
{
	private Map<Skill, Integer> skillsMap;
	private EntityPlayer player;

	public SkillStats(EntityPlayer p)
	{
		player = p;
		skillsMap = new HashMap<Skill, Integer>();
		for(Skill s : SkillsManager.instance.getSkillsArray())
		{
			setSkill(s.skillName, 0);
		}
	}

	public Object[] getSkillsKeysArray()
	{
		return skillsMap.keySet().toArray();
	}

	public void setSkill(String skillName, int amount)
	{
		Skill sk = SkillsManager.instance.getSkill(skillName);
		if(sk != null)
			skillsMap.put(sk, amount);
	}

	public void setSkillSave(String skillName, int amount)
	{
		Skill sk = SkillsManager.instance.getSkill(skillName);
		if(sk != null)
			skillsMap.put(sk, amount);
		writeNBT(player.getEntityData());
	}

	public void increaseSkill(String skillName, int amount)
	{
		Skill sk = SkillsManager.instance.getSkill(skillName);
		PlayerSkillEvent.Increase event = new PlayerSkillEvent.Increase(this.player, skillName, amount);
		if(!MinecraftForge.EVENT_BUS.post(event))
		{
			if(skillsMap.containsKey(sk))
			{
				//First get what the skill level currently is
				int i = skillsMap.get(sk);
				//The put method replaces the old identical entry.
				skillsMap.put(sk, i+amount);
			}
			else
			{
				skillsMap.put(sk, amount);
			}
		}

		int i = skillsMap.get(sk);
		if(player instanceof EntityPlayerMP)
		{
			AbstractPacket pkt = new PlayerUpdatePacket(1, skillName, i);
			TerraFirmaCraft.PACKET_PIPELINE.sendTo(pkt, (EntityPlayerMP) player);
		}
		writeNBT(player.getEntityData());
	}

	public int getSkillRaw(String skillName)
	{
		Skill sk = SkillsManager.instance.getSkill(skillName);
		if(skillsMap.containsKey(sk))
			return skillsMap.get(sk);
		else
			return 0;
	}

	public SkillRank getSkillRank(String skillName)
	{
		float raw = getSkillMultiplier(skillName);
		if(raw < 0.0625)
		{
			return SkillRank.Dabbling;
		}
		else if(raw < 0.0625 * 2)
		{
			return SkillRank.Novice;
		}
		else if(raw < 0.0625 * 3)
		{
			return SkillRank.Adequate;
		}
		else if(raw < 0.0625 * 4)
		{
			return SkillRank.Competent;
		}
		else if(raw < 0.0625 * 5)
		{
			return SkillRank.Skilled;
		}
		else if(raw < 0.0625 * 6)
		{
			return SkillRank.Proficient;
		}
		else if(raw < 0.0625 * 7)
		{
			return SkillRank.Talented;
		}
		else if(raw < 0.0625 * 8)
		{
			return SkillRank.Adept;
		}
		else if(raw < 0.0625 * 9)
		{
			return SkillRank.Expert;
		}
		else if(raw < 0.0625 * 10)
		{
			return SkillRank.Professional;
		}
		else if(raw < 0.0625 * 11)
		{
			return SkillRank.Accomplished;
		}
		else if(raw < 0.0625 * 12)
		{
			return SkillRank.Great;
		}
		else if(raw < 0.0625 * 13)
		{
			return SkillRank.Master;
		}
		else if(raw < 0.0625 * 14)
		{
			return SkillRank.HighMaster;
		}
		else if(raw < 0.0625 * 15)
		{
			return SkillRank.GrandMaster;
		}
		else
		{
			return SkillRank.Legendary;
		}
	}

	public float getPercToNextRank(String skillName)
	{
		float raw = getSkillMultiplier(skillName);
		if(raw < 0.0625)
		{
			return raw/0.0625f;
		}
		else if(raw < 0.0625 * 2)
		{
			return (raw-0.0625f)/0.0625f;
		}
		else if(raw < 0.0625 * 3)
		{
			return (raw-0.0625f*2)/0.0625f;
		}
		else if(raw < 0.0625 * 4)
		{
			return (raw-0.0625f*3)/0.0625f;
		}
		else if(raw < 0.0625 * 5)
		{
			return (raw-0.0625f*4)/0.0625f;
		}
		else if(raw < 0.0625 * 6)
		{
			return (raw-0.0625f*5)/0.0625f;
		}
		else if(raw < 0.0625 * 7)
		{
			return (raw-0.0625f*6)/0.0625f;
		}
		else if(raw < 0.0625 * 8)
		{
			return (raw-0.0625f*7)/0.0625f;
		}
		else if(raw < 0.0625 * 9)
		{
			return (raw-0.0625f*8)/0.0625f;
		}
		else if(raw < 0.0625 * 10)
		{
			return (raw-0.0625f*9)/0.0625f;
		}
		else if(raw < 0.0625 * 11)
		{
			return (raw-0.0625f*10)/0.0625f;
		}
		else if(raw < 0.0625 * 12)
		{
			return (raw-0.0625f*11)/0.0625f;
		}
		else if(raw < 0.0625 * 13)
		{
			return (raw-0.0625f*12)/0.0625f;
		}
		else if(raw < 0.0625 * 14)
		{
			return (raw-0.0625f*13)/0.0625f;
		}
		else if(raw < 0.0625 * 15)
		{
			return (raw-0.0625f*14)/0.0625f;
		}
		else
		{
			return (raw-0.0625f*15)/0.0625f;
		}
	}

	public float getSkillMultiplier(String skillName)
	{
		int skill = getSkillRaw(skillName);
		Skill sk = SkillsManager.instance.getSkill(skillName);
		float mult = getSkillMult(skill, sk.skillRate);
		GetSkillMultiplierEvent event = new GetSkillMultiplierEvent(player, skillName, mult);
		MinecraftForge.EVENT_BUS.post(event);
		return event.skillResult;
	}

	private float getSkillMult(int skill, float rate)
	{
		return 1 - (rate / (rate + skill));
	}

	public void readNBT(NBTTagCompound nbt)
	{
		if (nbt.hasKey("skillCompound"))
		{
			NBTTagCompound skillCompound = nbt.getCompoundTag("skillCompound");
			for(Object n : skillCompound.func_150296_c())
			{
				String skill = (String) n;
				setSkill(skill, skillCompound.getInteger(skill));
			}
		}
	}

	/**
	 * Writes food stats to an NBT object.
	 */
	public void writeNBT(NBTTagCompound nbt)
	{
		NBTTagCompound skillCompound = new NBTTagCompound();
		Object[] keys = skillsMap.keySet().toArray();
		for(Object o : keys)
		{
			Skill k = (Skill)o;
			int f = skillsMap.get(k);
			skillCompound.setInteger(k.skillName, f);
		}
		nbt.setTag("skillCompound", skillCompound);
	}

	public void toOutBuffer(ByteBuf buffer)
	{
		Object[] keys = skillsMap.keySet().toArray();
		buffer.writeInt(keys.length);
		for(Object o : keys)
		{
			Skill k = (Skill)o;
			int f = skillsMap.get(k);
			ByteBufUtils.writeUTF8String(buffer, k.skillName);
			buffer.writeInt(f);
		}
	}

	public enum SkillRank
	{
		Dabbling("gui.skill.dabbling"), Novice("gui.skill.novice"), Adequate("gui.skill.adequate"), Competent("gui.skill.competent"),
		Skilled("gui.skill.skilled"), Proficient("gui.skill.proficient"), Talented("gui.skill.talented"), Adept("gui.skill.adept"),
		Expert("gui.skill.expert"), Professional("gui.skill.professional"), Accomplished("gui.skill.accomplished"), Great("gui.skill.great"),
		Master("gui.skill.master"), HighMaster("gui.skill.highmaster"), GrandMaster("gui.skill.grandmaster"), Legendary("gui.skill.legendary");

		String name;
		private SkillRank(String local)
		{
			name = local;
		}

		public String getUnlocalizedName()
		{
			return name;
		}

		public String getLocalizedName()
		{
			return TFC_Core.translate(name);
		}
	}

}
