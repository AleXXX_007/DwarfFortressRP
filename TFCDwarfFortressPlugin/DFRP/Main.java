package DFRP;

import org.bukkit.plugin.java.JavaPlugin;

import DFRP.Chat.Emote;
import DFRP.Chat.EmoteImpersonal;
import DFRP.Chat.OOC_Global;
import DFRP.Chat.OOC_Local;
import DFRP.Chat.PersonalMessage;
import DFRP.Chat.TalkEvent;
import DFRP.Chat.Whisper;
import DFRP.Chat.Yell;
import DFRP.Chat.Utils.SetColor;
import DFRP.Commands.Event;
import DFRP.Commands.Migrants;
import DFRP.Commands.Slay;
import DFRP.Listeners.DamageListener;
import DFRP.Listeners.JoinListener;
import DFRP.Listeners.WeatherListener;

public class Main extends JavaPlugin {

	public void onEnable() {
		getConfig().options().copyDefaults(true);
		saveConfig();

		// Listeners
		new JoinListener(this);
		new DamageListener(this);
		new WeatherListener(this);

		// Commands
		new Event(this);
		new Migrants(this);
		new Slay(this);

		// Chat
		new OOC_Local(this);
		new OOC_Global(this);
		new Emote(this);
		new EmoteImpersonal(this);
		new PersonalMessage(this);
		new Whisper(this);
		new Yell(this);
		new TalkEvent(this);
		new SetColor(this);
	}

	public void onDisable() {
		saveConfig();
	}
}