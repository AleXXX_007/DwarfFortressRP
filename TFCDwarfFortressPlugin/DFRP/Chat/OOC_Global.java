package DFRP.Chat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;

public class OOC_Global implements CommandExecutor {

	private Map<String, String> map = new HashMap<String, String>();
	public String format;
	public Logger logger;
	public FileConfiguration config;

	public OOC_Global(Main plugin) {
		plugin.getCommand("ooc").setExecutor(this);
		logger = plugin.getLogger();
		config = plugin.getConfig();
		format = config.getString("chat.ooc_global.format");
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ooc")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					Player player = (Player) sender;
					Server server = sender.getServer();
					String message = String.join(" ", args);
					map.put("sender", Colorer.getColoredName(player));
					map.put("message", message);

					server.broadcastMessage(StrSubstitutor.replace(format, map));

					logger.info(String.format("[OOC]%s: %s", player.getName(), message));
				}
			}
			return true;
		}
		return false;
	}
}
