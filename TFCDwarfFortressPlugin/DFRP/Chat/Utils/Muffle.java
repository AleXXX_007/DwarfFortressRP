package DFRP.Chat.Utils;

import java.util.Random;

import org.bukkit.ChatColor;

public class Muffle {
	private static Random random = new Random();

	private static void fadeAround(String[] msgarray, int i) {
		if (i - 2 >= 0) {
			msgarray[i - 2] = ChatColor.GRAY + msgarray[i - 2];
		}
		if (i - 1 >= 0) {
			msgarray[i - 1] = ChatColor.DARK_GRAY + msgarray[i - 1];
		}
		if (i + 1 <= msgarray.length - 1) {
			msgarray[i + 1] = ChatColor.DARK_GRAY + msgarray[i + 1];
		}
		if (i + 2 <= msgarray.length - 1) {
			msgarray[i + 2] = ChatColor.GRAY + msgarray[i + 2];
		}
	}

	public static String muffle(String string, double distance, int range, int muffle) {
		String[] msgarray = string.split("");
		int i;
		for (i = 0; i < msgarray.length; i++) {
			if (random.nextInt((int) Math.ceil(distance)) > muffle) {
				msgarray[i] = " ";
				fadeAround(msgarray, i);
			}
		}
		return String.join("", msgarray) + ChatColor.WHITE;
	}
	/*
	 * public static void muffle(String[] args, String format, Player player,
	 * int range, int muffle) { String message = String.join(" ", args);
	 * player.sendMessage(String.format(format, Helper.ColorString(player),
	 * message));
	 * 
	 * for (Entity entity: player.getNearbyEntities(range, range, range)) { if
	 * (entity instanceof Player) { Player reciever = (Player) entity; double
	 * distance = player.getLocation().distance(reciever.getLocation());
	 * 
	 * if (distance < muffle) { reciever.sendMessage(String.format(format,
	 * Helper.ColorString(player), message));
	 * 
	 * 
	 * } else if (distance <= range) { String[] msgarray = message.split("");
	 * int i; for (i = 0; i < msgarray.length; i++) { if (random.nextInt((int)
	 * Math.ceil(distance)) > muffle) { msgarray[i] = " "; fadeAround(msgarray,
	 * i); } }
	 * 
	 * 
	 * String msg = String.join("", msgarray);
	 * reciever.sendMessage(String.format(format, Helper.ColorString(player),
	 * msg)); } } } }
	 */
	/*
	 * public static void muffle(String[] args, String format, Player player,
	 * int range, int muffle) { String message = ""; for (String s: args) {
	 * message += s + " "; } player.sendMessage(String.format(format,
	 * player.getName(), message)); for (Entity entity:
	 * player.getNearbyEntities(range, range, range)) { if (entity instanceof
	 * Player) { Player reciever = (Player) entity; double distance =
	 * player.getLocation().distance(reciever.getLocation());
	 * 
	 * if (distance < muffle) { reciever.sendMessage(String.format(format,
	 * player.getName(), message));
	 * 
	 * 
	 * } else if (distance <= range) { String msg = ""; for (char ch:
	 * message.toCharArray()){ if (random.nextInt((int) distance) + 1 > range -
	 * muffle) { msg += " "; } else if (random.nextInt((int) distance) + 1 >
	 * range - muffle) { int rand = random.nextInt(2); if (rand == 0) { msg +=
	 * ChatColor.GRAY + String.valueOf(ch); } else if (rand == 1) { msg +=
	 * ChatColor.DARK_GRAY + String.valueOf(ch); } } else { msg +=
	 * String.valueOf(ch); } } reciever.sendMessage(String.format(format,
	 * player.getName(), msg)); } } } }
	 */
}
