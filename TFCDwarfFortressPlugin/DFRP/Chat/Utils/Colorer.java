package DFRP.Chat.Utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Colorer {
	private static File file = new File("plugins/Dwarf_Fortress_RP", "players.yml");
	private static FileConfiguration playersconfig = YamlConfiguration.loadConfiguration(file);

	public static ChatColor getColor(String colorstring) {
		String colorstr = colorstring;
		ChatColor color = null;
		switch (colorstr.toLowerCase()) {
		case "aqua":
			color = ChatColor.AQUA;
			break;
		case "black":
			color = ChatColor.BLACK;
			break;
		case "blue":
			color = ChatColor.BLUE;
			break;
		case "dark aqua":
			color = ChatColor.DARK_AQUA;
			break;
		case "dark blue":
			color = ChatColor.DARK_BLUE;
			break;
		case "dark gray":
			color = ChatColor.DARK_GRAY;
			break;
		case "dark green":
			color = ChatColor.DARK_GREEN;
			break;
		case "dark purple":
			color = ChatColor.DARK_PURPLE;
			break;
		case "dark red":
			color = ChatColor.DARK_RED;
			break;
		case "gold":
			color = ChatColor.GOLD;
			break;
		case "gray":
			color = ChatColor.GRAY;
			break;
		case "green":
			color = ChatColor.GREEN;
			break;
		case "light purple":
			color = ChatColor.LIGHT_PURPLE;
			break;
		case "red":
			color = ChatColor.RED;
			break;
		case "white":
			color = ChatColor.WHITE;
			break;
		case "yellow":
			color = ChatColor.YELLOW;
		}
		return color;
	}

	public static String getAllColors() {
		return ChatColor.AQUA + "Aqua, " + ChatColor.BLACK + "Black, " + ChatColor.BLUE + "Blue, " + ChatColor.DARK_AQUA
				+ "Dark Aqua, " + ChatColor.DARK_BLUE + "Dark Blue, " + ChatColor.DARK_GRAY + "Dark Gray, "
				+ ChatColor.DARK_GREEN + "Dark Green, " + ChatColor.DARK_PURPLE + "Dark Purple, " + ChatColor.DARK_RED
				+ "Dark Red, " + ChatColor.GOLD + "Gold, " + ChatColor.GRAY + "Gray, " + ChatColor.GREEN + "Green, "
				+ ChatColor.LIGHT_PURPLE + "Light Purple, " + ChatColor.RED + "Red, " + ChatColor.WHITE + "White, "
				+ ChatColor.YELLOW + "Yellow.";
	}

	public static void setColor(Player player, String value) throws IOException {
		// Set value to player config
		playersconfig.set(String.format("%s.%s.%s", "players", player.getName(), "color"), value);
		playersconfig.save(file);
	}

	public static String getColor(Player player) {
		// Get value from player config
		String color = playersconfig.getString(String.format("%s.%s.%s", "players", player.getName(), "color", null));
		if (color != null) {
			return color;
		} else {
			return null;
		}
	}

	public static String getColoredName(Player p) {
		// Returns colored playername
		String str = p.getName();
		if (getColor(p) != null) {
			str = getColor(getColor(p)) + str + ChatColor.WHITE;
		}
		return str;
	}

}
