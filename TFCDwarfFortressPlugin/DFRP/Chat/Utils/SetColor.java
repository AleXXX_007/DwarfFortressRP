package DFRP.Chat.Utils;

import java.io.IOException;
import java.util.Arrays;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;

public class SetColor implements CommandExecutor {

	public SetColor(Main plugin) {
		plugin.getCommand("setcolor").setExecutor(this);
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("setcolor")) {
			if (sender instanceof Player) {
				if (args.length >= 2) {
					@SuppressWarnings("deprecation")
					Player player = sender.getServer().getPlayer(args[0]);
					String color = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
					if (!player.equals(null)) {

						if (Colorer.getColor(color) != null) {
							try {
								Colorer.setColor(player, color);
							} catch (IOException e) {
							}
							sender.sendMessage(String.format("����� ���� ������ %s - %s", player.getName(),
									Colorer.getColor(color) + color));
						} else {
							sender.sendMessage("Invalid Color, avaliable colors:\n" + Colorer.getAllColors());
						}
					}
				}
				sender.sendMessage("Avaliable colors:\n" + Colorer.getAllColors());
			}
			return true;
		}
		return false;
	}
}
