package DFRP.Chat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;
import DFRP.Chat.Utils.Muffle;

public class Yell implements CommandExecutor {

	private Map<String, String> map = new HashMap<String, String>();
	public int range, muffle;
	public String format;
	public Logger logger;
	public FileConfiguration config;

	public Yell(Main plugin) {
		plugin.getCommand("yell").setExecutor(this);
		logger = plugin.getLogger();
		config = plugin.getConfig();

		format = config.getString("chat.yell.format");
		range = config.getInt("chat.yell.range");
		muffle = config.getInt("chat.yell.muffle");

	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("yell")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					Player player = (Player) sender;
					String message = String.join(" ", args);
					map.put("sender", Colorer.getColoredName(player));

					for (Entity entity : player.getNearbyEntities(range, range, range)) {
						if (entity instanceof Player) {
							Player reciever = (Player) entity;
							double distance = player.getLocation().distance(reciever.getLocation());

							if (distance < muffle) {
								map.put("message", message);
								reciever.sendMessage(StrSubstitutor.replace(format, map));
							} else if (distance > muffle) {
								map.put("message", Muffle.muffle(message, distance, range, muffle));
								reciever.sendMessage(StrSubstitutor.replace(format, map));
							}
						}
					}
					map.put("message", message);
					player.sendMessage(StrSubstitutor.replace(format, map));

					logger.info(String.format("[IC]%s yells: %s", player.getName(), message));
				}
			}
			return true;
		}
		return false;
	}
}
