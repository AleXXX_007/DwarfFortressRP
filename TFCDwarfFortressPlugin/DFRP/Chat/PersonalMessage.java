package DFRP.Chat;

import java.util.Arrays;
import java.util.logging.Logger;

import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;

public class PersonalMessage implements CommandExecutor {

	public String format;
	public Logger logger;
	public FileConfiguration config;

	public PersonalMessage(Main plugin) {
		plugin.getCommand("pm").setExecutor(this);
		logger = plugin.getLogger();
		format = "[%s->%s] %s";
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pm")) {
			if (sender instanceof Player) {
				if (args.length >= 2) {
					Server server = sender.getServer();
					Player reciever = server.getPlayer(args[0]);
					Player player = (Player) sender;

					if (reciever == null) {
						player.sendMessage("Player is not online.");
						return false;
					} else {
						String message = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

						player.sendMessage(String.format(format, Colorer.getColoredName(player),
								Colorer.getColoredName(reciever), message));
						reciever.sendMessage(String.format(format, Colorer.getColoredName(player),
								Colorer.getColoredName(reciever), message));

						logger.info(String.format(format, player.getName(), reciever.getName(), message));
					}
				}

			}
			return true;
		}
		return false;
	}
}
