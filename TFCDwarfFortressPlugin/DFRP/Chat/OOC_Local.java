package DFRP.Chat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;

public class OOC_Local implements CommandExecutor {

	private Map<String, String> map = new HashMap<String, String>();
	public int range;
	public String format;
	public Logger logger;
	public FileConfiguration config;

	public OOC_Local(Main plugin) {
		plugin.getCommand("looc").setExecutor(this);
		logger = plugin.getLogger();
		config = plugin.getConfig();

		range = config.getInt("chat.ooc_local.range");
		format = config.getString("chat.ooc_local.format");
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("looc")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					Player player = (Player) sender;
					String message = String.join(" ", args);
					map.put("sender", Colorer.getColoredName(player));

					for (Entity entity : player.getNearbyEntities(range, range, range)) {
						if (entity instanceof Player) {
							Player reciever = (Player) entity;
							map.put("message", message);

							reciever.sendMessage(StrSubstitutor.replace(format, map));
						}
					}
					player.sendMessage(StrSubstitutor.replace(format, map));

					logger.info(String.format("[OOC]%s: %s", player.getName(), message));
				}
			}
			return true;
		}
		return false;
	}
}
