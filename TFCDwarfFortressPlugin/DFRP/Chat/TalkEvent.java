package DFRP.Chat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;
import DFRP.Chat.Utils.Muffle;

public class TalkEvent implements Listener {

	private Map<String, String> map = new HashMap<String, String>();
	private int range, muffle;
	private String format;
	private Logger logger;
	private FileConfiguration config;

	public TalkEvent(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		logger = plugin.getLogger();
		config = plugin.getConfig();

		format = config.getString("chat.talk.format");
		range = config.getInt("chat.talk.range");
		muffle = config.getInt("chat.talk.muffle");
	}

	@EventHandler
	public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		String message = e.getMessage();
		e.setCancelled(true);
		map.put("sender", Colorer.getColoredName(player));

		for (Entity entity : player.getNearbyEntities(range, range, range)) {
			if (entity instanceof Player) {
				Player reciever = (Player) entity;
				double distance = player.getLocation().distance(reciever.getLocation());

				if (distance < muffle) {
					map.put("message", message);
					reciever.sendMessage(StrSubstitutor.replace(format, map));
				} else if (distance > muffle) {
					map.put("message", Muffle.muffle(message, distance, range, muffle));
					reciever.sendMessage(StrSubstitutor.replace(format, map));
				}
			}
		}
		map.put("message", message);
		player.sendMessage(StrSubstitutor.replace(format, map));

		logger.info(String.format("[IC]%s talks: %s", player.getName(), message));
	}
}
