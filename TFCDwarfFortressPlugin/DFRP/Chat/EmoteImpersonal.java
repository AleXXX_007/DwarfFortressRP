package DFRP.Chat;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.text.StrSubstitutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;

public class EmoteImpersonal implements CommandExecutor {

	private Map<String, String> map = new HashMap<String, String>();
	public int range;
	public String format;
	public Logger logger;
	public FileConfiguration config;

	public EmoteImpersonal(Main plugin) {
		plugin.getCommand("it").setExecutor(this);
		logger = plugin.getLogger();
		config = plugin.getConfig();

		format = config.getString("chat.emote_impersonal.format");
		range = config.getInt("chat.emote_impersonal.range");
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("it")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					Player player = (Player) sender;
					String message = String.join(" ", args);
					map.put("message", message);

					for (Entity entity : player.getNearbyEntities(range, range, range)) {
						if (entity instanceof Player) {
							Player reciever = (Player) entity;

							reciever.sendMessage(StrSubstitutor.replace(format, map));
						}
					}
					player.sendMessage(StrSubstitutor.replace(format, map));
					logger.info(String.format(format, message) + ", sent by " + sender.getName());
				}
			}
			return true;
		}
		return false;
	}
}
