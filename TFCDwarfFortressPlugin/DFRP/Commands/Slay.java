package DFRP.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;

public class Slay implements CommandExecutor {
	public Slay(Main plugin) {
		plugin.getCommand("slay").setExecutor(this);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("slay")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					for (String p : args) {
						sender.getServer().getPlayer(p).setHealth(0.0);
					}
				}
			} else {
				sender.sendMessage("Usage: /slay <Name> ... [Name]");
			}
			return true;
		}
		return false;
	}
}
