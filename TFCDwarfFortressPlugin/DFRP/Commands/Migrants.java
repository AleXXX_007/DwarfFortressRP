package DFRP.Commands;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import DFRP.Main;

public class Migrants implements CommandExecutor {
	private ArrayList<String> migrants = new ArrayList<String>();
	private int i;

	public Migrants(Main plugin) {
		plugin.getCommand("migrants").setExecutor(this);
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("migrants")) {
			if (sender instanceof Player) {
				if (args.length > 0) {
					if (args[0].equalsIgnoreCase("add")) {
						if (((args.length - 1) % 2 == 0) && ((args.length - 1) > 0)) {
							for (i = 1; i < args.length; i += 2) {
								migrants.add(args[i].replaceAll("_", " "));
								migrants.add(args[i + 1].replaceAll("_", " "));
							}
						} else {
							sender.sendMessage("Usage: /migrants <Name_SubName> <Profession>");
						}

					} else if (args[0].equalsIgnoreCase("clear")) {
						migrants.clear();
						sender.sendMessage("List has been cleared");

					} else if (args[0].equalsIgnoreCase("send")) {
						Server server = sender.getServer();
						if (migrants.size() != 0) {
							for (i = 0; i < migrants.size(); i += 2) {
								server.broadcastMessage(ChatColor.DARK_PURPLE
										+ String.format("%s, %s has arrived.", migrants.get(i), migrants.get(i + 1)));
							}
							migrants.clear();
							server.broadcastMessage(ChatColor.GRAY + "Some migrants have arrived");
						} else {
							sender.sendMessage("A list of migrants is empty, use /migrants add");
						}
					} else {
						sender.sendMessage("Usage: /migrants <Name_SubName> <Profession>");
					}
				} else {
					sender.sendMessage("Avaliable commands:\n/migrants add\n/migrants send");
				}
			}
			return true;
		}
		return false;
	}
}
