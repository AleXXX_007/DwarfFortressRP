package DFRP.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;

import DFRP.Main;

public class Event implements CommandExecutor {
	public Event(Main plugin) {
		plugin.getCommand("event").setExecutor(this);
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("event")) {
			Server server = sender.getServer();
			String msg = "";
			for (String s : args) {
				msg += s;
				msg += " ";
			}
			server.broadcastMessage(ChatColor.GOLD + msg);
			return true;
		}
		return false;
	}
}
