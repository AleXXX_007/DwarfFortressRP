package DFRP.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import DFRP.Main;
import DFRP.Chat.Utils.Colorer;

public class JoinListener implements Listener {
	public JoinListener(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		e.setJoinMessage(String.format("[OOC]%s has joined", Colorer.getColoredName(e.getPlayer())));
	}
}