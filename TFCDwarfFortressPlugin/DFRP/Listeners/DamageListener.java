package DFRP.Listeners;

import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import DFRP.Main;

public class DamageListener implements Listener {

	HashMap<String, Boolean> IsDown = new HashMap<String, Boolean>();
	FileConfiguration config = null;
	public int MinHealth;
	public int[] KnockoutMessageRange = new int[2];

	public DamageListener(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.config = plugin.getConfig();
		this.KnockoutMessageRange[0] = config.getInt("player.knockout.horizontal");
		this.KnockoutMessageRange[1] = config.getInt("player.knockout.vertical");
		this.MinHealth = config.getInt("player.minhealth");
	}

	@EventHandler
	public void onDamageEvent(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			double health = p.getHealth();
			String playername = p.getName();

			if (health < MinHealth) {
				p.setHealth(MinHealth);
			}

			if (health < 100) {
				boolean p_isdown = IsDown.get(playername);
				if (!p_isdown) {
					IsDown.put(playername, true);
					p.sendMessage(ChatColor.DARK_RED + "�� �������� ��������.");
					for (Entity entity : p.getNearbyEntities(KnockoutMessageRange[0], KnockoutMessageRange[1],
							KnockoutMessageRange[0])) {
						if (entity instanceof Player) {
							Player nearPlayer = (Player) entity;
							nearPlayer.sendMessage(ChatColor.DARK_RED + playername + " ������� ��������.");
						}
					}
				}
				IsDown.put(playername, true);
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 6));
				p.removePotionEffect(PotionEffectType.BLINDNESS);
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 1));
				p.removePotionEffect(PotionEffectType.WEAKNESS);
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 200, 63));
				p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 4));
			}
		}
	}

	@EventHandler
	public void onRegainHealth(EntityRegainHealthEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			double health = p.getHealth();
			String playername = p.getName();
			if (health > 100) {
				if (IsDown.get(playername) == null) {
					IsDown.put(playername, false);
				} else if (IsDown.get(playername)) {
					IsDown.put(playername, false);
				}
			}
			if (health < 100) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 6));
				p.removePotionEffect(PotionEffectType.BLINDNESS);
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 1));
				p.removePotionEffect(PotionEffectType.WEAKNESS);
				p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 200, 9));
				p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 4));
			} else if (health < 250) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.removePotionEffect(PotionEffectType.BLINDNESS);
				p.removePotionEffect(PotionEffectType.WEAKNESS);
				p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 5));
			} else if (health < 400) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 4));
			} else if (health < 550) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 3));
			} else if (health < 700) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 2));
			} else if (health < 850) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1));
			} else if (health < 1000) {
				p.removePotionEffect(PotionEffectType.SLOW);
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 0));
			} else if (health > 1000) {
				p.removePotionEffect(PotionEffectType.SLOW);
			}
		}
	}
}