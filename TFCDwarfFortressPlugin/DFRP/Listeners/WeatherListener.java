package DFRP.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import DFRP.Main;

public class WeatherListener implements Listener {
	public WeatherListener(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		World world = event.getWorld();
		if (event.toWeatherState()) {
			for (Player player : world.getPlayers()) {
				player.sendMessage(ChatColor.AQUA + "It has started raining");
			}
		} else if (!event.toWeatherState()) {
			for (Player player : world.getPlayers()) {
				player.sendMessage(ChatColor.GRAY + "The weather has cleared");
			}
		}
	}
}